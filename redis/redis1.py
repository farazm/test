import subprocess
import time
import atexit

def terminator(proc):
     proc.terminate()

proc = subprocess.Popen(['redis-server'])
time.sleep(5)

atexit.register(terminator(proc))

