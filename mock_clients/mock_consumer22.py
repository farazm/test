import confluent_kafka
import time
from confluent_kafka import Consumer, Producer, KafkaError

consumer = Consumer({'bootstrap.servers': 'localhost:29092', 'group.id': 'xyz1'})
consumer.subscribe(['test1'])
while True:
    message = consumer.poll(3)
    if message is None:
        continue
    else:
        print '############################\nvalue'
        print message.value()
        print 'partition'
        print message.partition()
        print 'offset'
        print message.offset()         
