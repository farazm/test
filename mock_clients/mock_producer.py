import confluent_kafka
import time
from confluent_kafka import Consumer, Producer, KafkaError
import random
import json

#message = {"rectifier_door_status": "", "main_packets_sent": "C898", "ats_kwh_load": "10753.15", "batter_bank_door_status": "", "ip": "10.225.28.137", "packet_sequince_number": "47060", "string_4_battery_4_value": "", "gsm_signal_strength": "2715", "ats_kvarh_load": "1279.48", "datetime": "2017-07-13 12:29:27", "temperature_2_value": "", "string_1_battery_1_value": "", "dci_battery_bank": "", "power_source_status": "1", "ac_cutgen_test_feedback": "0", "temperature_3_value": "", "delay_gen_feedback": "0", "port": "4001", "last_reset_cause": "7", "bam_bank_1_temp": "", "fuel_enclosure": "0", "fuel_signal": "9", "string_2_battery_4_value": "", "string_1_battery_4_value": "", "comm_controller_reset_counter": "0002", "spare_ac_current_ports": "0", "fuel_temperature": "9.76", "bam_dsid": "", "string_2_battery_2_value": "", "number_of_acks_revceived": "C8E0", "ats_frequency": "50.2", "string_4_battery_1_value": "", "string_3_battery_1_value": "", "memory_card_status_port": "0", "time_stamp": "1499948967", "dsid_i": "28FF53E46216039A", "eeprom_checksum_newold": "5050", "string_1_battery_3_value": "", "epa1": "41", "epa2": "40", "epa3": "40", "main_controller_reset_counter": "94", "total_packet_sent": "C416", "string_4_battery_3_value": "", "ats_phase_current_1": "5.04", "string_3_battery_4_value": "", "arrivaltime": "2017-07-13 17:29:31", "ats_phase_voltage3": "248.39", "ats_phase_voltage2": "248.28", "tbv": "54.04", "ats_power_factor_p3": "1", "bam_bank_3_temp": "", "gen_status": "0", "string_2_battery_3_value": "", "cp_relay_status": "0", "date": "2017-07-13", "string_4_battery_2_value": "", "temperature_1_value": "", "ats_phase_current_2": "4.86", "gen_phase_3": "5", "spare_dci": "", "ats_power_factor_p2": "0.98", "rtu": "89898920299", "on_board_temperature": "39", "ats_power_factor_p1": "0.98", "string_1_battery_2_value": "", "on_board_temperature_value": "", "string_3_battery_2_value": "", "string_2_battery_1_value": "", "rbv": "13.41", "main_controller_firmware_version": "M6.04", "acp_status": "0", "gbv": "0.24", "dci_rectifier_load": "14.84", "ats_phase_voltage1": "244.02", "gen_phase_1": "5", "gen_phase_2": "5", "time": "12:29:27", "bam_bank_2_temp": "", "db_connector_door": "1", "db_mcu_door": "1", "ats_phase_current_3": "0", "sms_sent_count": "2887", "string_3_battery_3_value": ""}
messages = []
messages.append({"datetime": "2017-07-13 12:29:27", "ats_phase_voltage1": "244.02", "ats_phase_current_1": "5.04",  "last_reset_cause": "7", "rtu": "89898920299"})
#messages.append({"datetime": "2017-07-13 12:29:27", "ats_phase_voltage1": "244.02", "ats_phase_current_1": "5.04",  "last_reset_cause": "7", "rtu": "89898920300"})
#messages.append({"datetime": "2017-07-13 12:29:27", "ats_phase_voltage1": "244.02", "ats_phase_current_1": "5.04",  "last_reset_cause": "7", "rtu": "89898920301"})
producer = Producer({'bootstrap.servers': 'localhost:29092'})
count = 0
while True:
    #message = input('enter message\n')
    for message in messages:
        message['ats_phase_voltage1'] = str(count)
        #message = str(count)
        count = count + 1
        producer.produce('test5', json.dumps(message), partition=1)
        print message
        time.sleep(random.randint(1,3)) 
        #raw_input('\n\nEnter')
