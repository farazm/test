import unittest
from datetime import datetime
from offline_detect import getdatetime

class test_app_offline_detect(unittest.TestCase):
    def test_negative_datetime_format (self):
        date_str = "2017-06-12"
        with self.assertRaises(ValueError):
            getdatetime(date_str)

    def test_datetime_conversion (self):
        date_str = "2017-06-12 10:30:42"
        timestamp = float(getdatetime(date_str))*60
        timestamp_to_time = datetime.fromtimestamp(timestamp)
        self.assertEqual(date_str, str(timestamp_to_time), msg=None)

    def getdatetime(date_str):
        validate_datetime(date_str)
        return float(datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S').strftime('%s'))/ 60

if __name__ == '__main__':
    unittest.main()
