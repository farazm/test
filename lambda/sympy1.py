from sympy import sympify
import parser

f = sympify('x**2 + y**2')
print (f.subs({'x':1, 'y':2}))


def _f(expression, template):
    code = parser.expr(expression).compile()
    return template(code)

def function_x(expression):
    return _f(expression, lambda code: lambda x: eval(code))

print dir(function_x('x**2 + 2*x + 4'))

print dir(function_x('x**2 + 2*x + 4').func_code)
