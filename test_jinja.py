import jinja2
import os

template_file = "template.j2"

output_directory = "_output"
name="Annelka"
env = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath="."))
template = env.get_template(template_file)
if not os.path.exists(output_directory):
    os.mkdir(output_directory)
result = template.render(name="Anelka")
f = open(os.path.join(output_directory, name + ".py"), "w")
f.write(result)
f.close()

